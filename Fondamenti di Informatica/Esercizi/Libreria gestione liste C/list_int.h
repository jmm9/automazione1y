#ifndef _LIST_INT_H
#ifndef _STDIO_H
#include <stdio.h>
#endif
#ifndef _STDLIB_H
#include <stdlib.h>
#endif
#define _LIST_INT_H
typedef struct lista_s {
	int val;
	struct lista_s * next;
} lista_t;

lista_t* push (lista_t *, int);
lista_t* append (lista_t *, int);
lista_t* insert (lista_t *, int, int, int);
lista_t* del (lista_t *, int);
lista_t* delPos (lista_t *, int, int);
lista_t* find (lista_t *, int);
lista_t* seekl (lista_t *, int, int);
int locc (lista_t *, int);
void printl (lista_t *);
int lstlen (lista_t *);
lista_t* empty (lista_t *);
lista_t * invertiLista (lista_t *);

lista_t* push (lista_t *head, int val)
{
	lista_t * tmp;
	
	if ((tmp = malloc (sizeof (lista_t)))) {
		tmp->val = val;
		tmp->next = head;
		head = tmp;
	} else {
		printf ("push: Errore allocazione memoria per %d\n", val);
	}
	
	return head;
}

lista_t* append (lista_t *head, int val)
{
	lista_t * tmp, * ptr;
	
	if ((tmp = malloc (sizeof (lista_t)))) {
		tmp->val = val;
		tmp->next = NULL;

		if (head) {
			ptr = head;
			while (ptr && ptr->next) {
				ptr = ptr->next;
			}
			ptr->next = tmp;
		} else {
			head = tmp;
		}
	} else {
		printf ("append: Errore allocazione memoria per %d\n", val);
	}
	
	return head;
}

lista_t* insert (lista_t *head, int val, int pos, int ordine)
{
	lista_t *tmp, *ptr;
	int i;

	if ((tmp = malloc (sizeof (lista_t)))) {
		tmp->val = val;
		ptr = head;
		for (i = 0; ptr; i++) {
			ptr = ptr->next;
		}
		if (ordine) {
			if (pos < 0) {
				pos = -pos;
			}
			pos = i - pos - 1;
		}
		if (pos >= 0 && pos <= i) {
			if (pos > 0) {
				ptr = head;
				for (i = 0; i < (pos - 1); i++) {
					ptr = ptr->next;
				}
				tmp->next = ptr->next;
				ptr->next = tmp;
			} else {
				tmp->next = head;
				head = tmp;
			}
		} else {
			printf ("insert: Posizione non valida\n");
		}
	} else {
		printf ("insert: Errore allocazione memoria per %d\n", val);
	}
	
	return head;
}

lista_t* del (lista_t *head, int val)
{
	lista_t *ptr, *del;
	
	while (head && head->val == val) {
		del = head;
		head = head->next;
		free (del);
	}

	ptr = head;
	while (ptr && ptr->next) {
		while (ptr->next && ptr->next->val == val) {
			del = ptr->next;
			ptr->next = del->next;
			free (del);
		}
		ptr = ptr->next;
	}

	return head;
}

lista_t* delPos (lista_t *head, int pos, int ordine)
{
	lista_t *ptr, *del;
	int i;
	
	ptr = head;
	for (i = 0; ptr; i++) {
		ptr = ptr->next;
	}
	if (ordine) {
		if (pos < 0) {
			pos = -pos;
		}
		pos = i - pos - 1;
	}
	if (pos >= 0 && pos <= i) {
		if (pos > 0) {
			ptr = head;
			for (i = 0; i < (pos - 1); i++) {
				ptr = ptr->next;
			}
			del = ptr->next;
			ptr->next = del->next;
			free (del);
		} else {
			del = head;
			head = del->next;
			free (del);
		}
	} else {
		printf ("delPos: Posizione non valida\n");
	}
	
	return head;
}

lista_t* find (lista_t *head, int val)
{
	lista_t *ris = NULL, *ptr;
	int trovato, i;

	ptr = head;
	trovato = 0;
	for (i = 0; ptr; i++) {
		if (ptr->val == val) {
			ris = append (ris, i);
			trovato++;
		}
		ptr = ptr->next;
	}
	if (!trovato) {
		printf ("find: Nessun elemento corrisponde ai criteri di ricerca (%d)\n", val);
	}

	return ris;
}

lista_t* seekl (lista_t *head, int pos, int ordine)
{
	lista_t *ris, *ptr;
	int i;

	ptr = head;
	for (i = 0; ptr; i++) {
		ptr = ptr->next;
	}
	if (ordine) {
		if (pos < 0) {
			pos = -pos;
		}
		pos = i - pos - 1;
	}
	if (pos >= 0 && pos <= i) {
		ptr = head;
		for (i = 0; i < pos; i++) {
			ptr = ptr->next;
		}
		ris = ptr;
	} else {
		ris = head;
		printf ("seekl: Posizione non valida\n");
	}

	return ris;
}

int locc (lista_t *head, int val)
{
	lista_t *ptr;
	int trovato;

	ptr = head;
	trovato = 0;
	while (ptr) {
		if (ptr->val == val) {
			trovato++;
		}
		ptr = ptr->next;
	}

	return trovato;
}

void printl (lista_t *head)
{
	if (!head) {
		printf ("printl: La lista e' vuota\n");
	}
	while (head) {
		printf ("%d ", head->val);
		head = head->next;
	}
	printf ("\n");
}

int lstlen (lista_t *head)
{
	int dim;

	for (dim = 0; head; dim++) {
		head = head->next;
	}

	return dim;
}

lista_t* empty (lista_t *head)
{
	lista_t *del;
	
	while (head) {
		del = head;
		head = del->next;
		free (del);
	}

	head = NULL;
	return head;
}

lista_t * invertiLista (lista_t *head)
{
	lista_t *lista = NULL;
	lista_t *tmp;

	tmp = head;
	while (tmp != NULL) {
		lista = push (lista, tmp->val);
		tmp = tmp->next;
	}

	head = empty (head);

	return lista;
}

lista_t * lstcpy (lista_t *source, lista_t *target)
{
	while (source) {
		target = append (target, source->val);
		source = source->next;
	}

	return target;
}
#endif
