/* Decomprime l'output della precedente */
#include <stdio.h>
#define CMAX (CMAX_ZIP / 2 * 9) /* Il / 2 è perché 25 è il massimo di lettere. Poi 9 è il massimo delle ripetizioni */
#define CMAX_ZIP 50
#define ZERO_CHAR '0'
#define NUM_NUMERI 9

int main(int argc, char * argv[])
{
	char input[CMAX_ZIP+1], output[CMAX+1];
	int i, j, lenUnZip, count;

	scanf ("%s", input);

	lenUnZip = 0;
	for (i = 0; input[i] != '\0'; i+=2) {
		count = input[i+1] - ZERO_CHAR;
		for (j = 0; j < count; j++) {
			output[lenUnZip] = input[i];
			lenUnZip++;
		}
	}

	output[lenUnZip] = '\0';
	printf("%s\n", &output);
	return 0;
}
