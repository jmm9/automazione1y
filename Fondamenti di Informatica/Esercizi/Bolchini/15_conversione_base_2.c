/* Inserito un numero tra 0 e 1023 estremi inclusi, stampa la codifica in base 2 */
#include <stdio.h>
#define NUMVAL 10 /* numero massimo di bit occupati */
#define BASE 2

int main(int argc, char * argv[])
{
	int bin[NUMVAL];
	int i, val, tmp;
	scanf("%d", &val);
	tmp = val;
	i = 0;
	do {
		bin [i] = tmp % BASE;
		tmp = tmp / BASE;
		i++;
	} while (tmp > 0);
	
	for (i--; i >= 0; i--) {
		printf("%d", bin[i]);
	}
	printf("\n");
	return 0;
}