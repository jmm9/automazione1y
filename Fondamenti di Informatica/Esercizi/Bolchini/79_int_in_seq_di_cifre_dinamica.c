/* Prende tutti i numeri primi presenti in un file e li stampa in un altro */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define BASE 10
#define CHAR_INIZ '0'

char * cifre (int);

int contacifre (int);

int main (int argc, char * argv[])
{
	int val;
	char *ris;

	scanf ("%d", &val);

	ris = cifre (val);

	printf ("%s\n", ris); /* Si può usare !!! */

	free (ris);
	return 0;
}


char * cifre (int val)
{
	char *ris;
	int dim, i, pot10;
	
	dim = contacifre (val);

	if ((ris = malloc ((dim+1) * sizeof (char)))) {
		pot10 = pow (BASE, dim - 1);
		for (i = 0; i < dim; i++) {
			*(ris + i) = (val / pot10) + CHAR_INIZ;
			val = val % pot10;
			pot10 = pot10 / BASE;
		}
		*(ris + i) = '\0';
	} else {
		printf ("Cifre: errore nell'allocare memoria per %d caratteri \n", (dim + 1));
	}	

	return ris;
}

int contacifre (int val)
{
	int ris;

	for (ris = 0; val > 0; ris++) {
		val = val / BASE;
	}

	return ris;
}