#include <stdio.h>
#include <stdlib.h>
#include "list_int.h"
#define STOP 0

lista_t * rimuoviDuplicati (lista_t *);

int main (int argc, char * argv[])
{
	lista_t * head = NULL;
	int val;

	scanf ("%d", &val);
	while (val != STOP) {
		head = append (head, val);
		scanf ("%d", &val);
	}

	head = rimuoviDuplicati (head);

	printl (head);

	head = empty (head);
	
	return 0;
}

lista_t * rimuoviDuplicati (lista_t * head)
{
	lista_t *ptr;
	int val;

	ptr = head;
	while (ptr && ptr->next) {
		val = ptr->val;
		ptr->next = del (ptr->next, val); /* delete li cancella tutti, quindi questo toglie l'elemento preso dalla lista presa a partire da quello dopo di lui */
		ptr = ptr->next;
	}

	return head;
}
