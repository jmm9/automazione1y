#include <stdio.h>
#define DIM 100
#define STOP 0

int main(int argc, char * argv[])
{
	int mat[DIM], i, j, trg, ris, dimEffettiva;
	scanf ("%d", &j);
	for (dimEffettiva = 0; j > STOP; dimEffettiva++) {
		mat[dimEffettiva] = j;
		scanf ("%d", &j);
	}
	do {
		scanf("%d", &trg);
	} while (trg <= STOP);
	ris = 0;
	for (i = 0; i < (dimEffettiva - 1) && ris == 0; i++) {
		for (j = i + 1; j < dimEffettiva && ris == 0; j++) {
			if (mat[i] + mat[j] == trg) {
				ris = 1;
			}
		}
	}
	printf("%d\n", ris);
	return 0;
}