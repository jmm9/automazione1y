#include <stdio.h>
#include <string.h>
#define CMAX 50
#define OPEN '('
#define CLOSE ')'

int valida (char []);
int massimo_annidamento (char []);

int main(int argc, char * argv[])
{
	char seq[CMAX+1];
	int ris;

	do {
		gets(seq);
	} while (!valida(seq));

	ris = massimo_annidamento(seq);

	printf("%d\n", ris);
	return 0;
}

int valida (char seq[])
{
	int annidamento, i, ris;

	annidamento = 0;
	for (i = 0; seq[i] != '\0' && annidamento >= 0; i++) {
		if (seq[i] == OPEN) {
			annidamento++;
		} else if (seq[i] == CLOSE) {
			annidamento--;
		}
	}
	
	if (annidamento == 0) {
		ris = 1;
	} else {
		ris = 0;
	}

	return ris;
}

int massimo_annidamento (char seq[])
{
	int annidamento, maxAnnidamento, i;

	annidamento = 0;
	maxAnnidamento = 0;
	for (i = 0; seq[i] != '\0'; i++) {
		if (seq[i] == OPEN) {
			annidamento++;
			if (annidamento > maxAnnidamento) {
				maxAnnidamento = annidamento;
			}
		} else if (seq[i] == CLOSE) {
			annidamento--;
		}
	}

	return maxAnnidamento;
}
