#include <stdio.h>
#include <math.h>
#define INPUT "./dati.txt"

typedef struct punto {
	float x, y;
} punto;

int main(int argc, char * argv[])
{
	FILE *fin;
	punto p1, p2;
	float lunghezza, max;

	if (fin = fopen (INPUT, "r")) {
		max = 0;
		while (!feof(fin)) {
			fscanf(fin, "%d", &p1.x);
			fscanf(fin, "%d", &p1.y);
			fscanf(fin, "%d", &p2.x);
			fscanf(fin, "%d", &p2.y);
			lunghezza = sqrt ((p2.x - p1.x)*(p2.x - p1.x)+(p2.y - p1.y)*(p2.y - p1.y));
			if (lunghezza > max) {
				max = lunghezza;
			}
		}
		fclose (fin);
		printf ("%d", max);
	} else {
		printf ("Problemi con il file %s\n", INPUT);
	}

	return 0;
}