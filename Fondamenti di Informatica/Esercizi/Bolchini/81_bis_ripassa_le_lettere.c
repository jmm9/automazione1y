#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define CAR_LEN 26
#define FILE_LEN 50
#define FIRST_LETTER 'a'
#define LAST_LETTER 'z'

int main (int argc, char * argv [])
{
	char caratteri [CAR_LEN+1], nomeFile [FILE_LEN+1], c;
	int i, dim, trovato;
	int *contatori;
	FILE *f;

	scanf ("%s", caratteri);
	scanf ("%s", nomeFile);

	dim = strlen (caratteri) - 1;
	if ((contatori = malloc (dim * sizeof (int)))) {
		for (i = 0; i <= dim; i++) {
			contatori[i] = 0;
		}
		if (f = fopen (nomeFile, "r")) {
			while (fscanf (f, "%c", &c) != EOF) {
				trovato = 0;
				for (i = 0; i <= dim && !trovato; i++) {
					if (c == caratteri[i]) {
						contatori [i]++;
						trovato++;
					}
				}
			}

			for (i = 0; i <= dim; i++) {
				printf ("%c: %d ", caratteri[i], contatori[caratteri[i] - FIRST_LETTER]);
			}
			fclose (f);
		} else {
			printf ("Errore nell'apertura del file %s\n", nomeFile);
		}
	} else {
		printf ("Errore allocazione memoria per %d interi\n", dim);
	}

	return 0;
}