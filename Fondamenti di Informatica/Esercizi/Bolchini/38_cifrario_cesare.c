/* ATTENZIONE: in C il resto può dare valori negativi */
#include <stdio.h>
#include <string.h>
#define LMAX 50
#define LETTERS 26
#define INIZIO 'a'

int main(int argc, char * argv[])
{
	char seq[LMAX+1], lettera;
	int dim, i, chiave, lettera_int;
	scanf("%s", seq);
	dim = strlen (seq);
	printf("Ora la chiave\n");
	scanf("%d", &chiave);
	for (i = 0; i < dim; i++) {
		lettera = seq[i];
		lettera_int = (lettera - INIZIO + chiave) % LETTERS;
		/*if (lettera_int < 0) {
			lettera_int = LETTERS + lettera_int;
		}*/
		lettera_int = (LETTERS + lettera_int) % LETTERS; /* Questa è la scorciatoia per non fare l'if, perché se è negativa poi il resto non fa nulla perché arriva su un numero <26, se invece è positivo agisce */
		lettera = INIZIO + lettera_int; /* % LETTERS in generale serve perché se metto ad esempio z (25) diventa 28 che è più delle 26 lettere. Se però faccio 28%26 trovo 2, cioè la lettera c a cui volevo arrivare */
		seq[i] = lettera;
	}
	printf("%s\n", seq);
	return 0;
}
