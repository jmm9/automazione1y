/*Acquisire un valore intero positivo e finchè non è tale lo richiede, quindi calcola e visualizza il numero di cifre che lo compongono*/
#include <stdio.h>
#define BASE 10

int main(int argc, char * argv[])
{
	int val, cif, ris;
	do {
		scanf("%d", &val);
	} while(val < 0);
	cif = val;
	ris = 0;
	do {
		ris++;
		cif = cif / BASE;
	} while (cif > 0);
	printf("%d\n", ris);
	return 0;
}