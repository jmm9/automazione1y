#include "list_char.h"
#define OPEN '('
#define CLOSE ')'
#define REPLACE '#'
#define CMAX 100

lista_t * togli_interno_parentesi (lista_t *);

int main (int argc, char * argv[])
{
	lista_t *head = NULL;
	int i;
	char seq[CMAX];
	
	scanf ("%s", seq);
	for (i = 0; seq[i] != '\0'; i++) {
		head = append (head, seq[i]);
	}

	head = togli_interno_parentesi (head);
	printl (head);
	head = empty (head);

	return 0;
}

/*lista_t * togli_interno_parentesi (lista_t *head)
{
	lista_t *ptr, *ris = NULL;
	
	ptr = head;
	while (ptr) {
		ris = append (ris, ptr->val);
		if (ptr->val == OPEN) {
			ris = append (ris, REPLACE);
			ptr = ptr->next;
			while (ptr->val != CLOSE) {
				ptr = ptr->next;
			}
			ris = append (ris, ptr->val);
		}
		ptr = ptr->next;
	}

	head = empty (head);

	return ris;
}*/

/*lista_t * togli_interno_parentesi (lista_t *head)
{
	lista_t *ptr;
	int i, j, k;
	
	ptr = head;
	for (i = 0; ptr; i++) {
		if (ptr->val == OPEN) {
			ptr = ptr->next;
			i++;
			head = insert (head, REPLACE, i, SEEK_SET);
			ptr = head;
			for (j = 0; j <= i || ptr->val != CLOSE; j++) {
				ptr = ptr->next;
			}
			for (k = 0; k < (j - i - 1); k++) {
				head = delPos (head, (i + 1), SEEK_SET);
			}
			ptr = head;
			for (j = 0; j <= i; j++) {
				ptr = ptr->next;
			}
			i++;
		}
		ptr = ptr->next;
	}

	return head;
}*/

/*lista_t * togli_interno_parentesi (lista_t *head)
{
	lista_t *tmp, *del, *ptr;
	
	ptr = head;
	while (ptr && ptr->next) {
		if (ptr->val == OPEN) {
			if(ptr->next->val != CLOSE) {
				ptr = ptr->next;
				ptr->val = REPLACE;
				while(ptr->next->val != CLOSE) {
					del = ptr->next;
					ptr->next = del->next;
					free(del);
				}
			} else {
				if ((tmp = malloc(sizeof(lista_t)))) {
					tmp->next = ptr->next;
					tmp->val = REPLACE;
					ptr->next = tmp;
				}
			}
		}
		ptr = ptr->next;
	}

	return head;
}*/

lista_t * togli_interno_parentesi (lista_t *head)
{
	lista_t *ptr, *tmp, *new;

	ptr = head;
	while (ptr) {
		if (ptr->val == OPEN) {
			tmp = ptr->next;
			while (tmp->val != CLOSE) {
				tmp = delPos (tmp, 0, SEEK_SET);
			}

			if ((new = malloc (sizeof (lista_t)))) {
				new->val = REPLACE;
				ptr->next = new;
				new->next = tmp;
			} else {
				printf ("Errore nell'allocazione memoria per %c\n", REPLACE);
			}
		}
		ptr = ptr->next;
	}

	return head;
}
