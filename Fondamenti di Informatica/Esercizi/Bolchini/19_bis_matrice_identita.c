/* Acquisire una matrice quadrata di dimensione 10. Calcolare se la matrice rappresentata è l'identità e visualizza 1, se no 0 */
#include <stdio.h>
#define DIM 10

int main(int argc, char * argv[])
{
	int mat[DIM][DIM];
	int i, j, identita;
	for (i = 0; i < DIM; i++) {
		for (j = 0; j < DIM; j++) {
			scanf ("%d", &mat[i][j]);
		}
	}
	identita = 1;
	for (i = 0; i < DIM && identita == 1; i++) {
		for (j = 0; j < DIM && identita == 1; j++) {
			if ((i == j && mat[i][j] != 1) || (i != j && mat[i][j] != 0)) {
				identita = 0;
			}
		}
	}
	printf ("%d\n", identita);
	return 0;
}
