/*Acquisisce 20 valori interi e un valore soglia. Calcola e visualizza quanti dei 20 superano strettamente la soglia*/
#include <stdio.h>
#define NUMVAL 20

int main(int argc, char * argv[])
{
	int val[NUMVAL];
	int i, soglia, count;
	i = 0;
	while (i < NUMVAL) {
		scanf("%d", &val[i]);
		i++;
	}
	scanf("%d", &soglia);
	i = 0;
	count = 0;
	while (i < NUMVAL) {
		if (val[i] > soglia) {
			count++;
		}
		i++;
	}
	printf("%d\n", count);
	return 0;
}