#include <stdio.h>
#define DIM 5
#define START 1
#define END 5

int proprieta_matrice (int [][DIM]);

int main (int argc, char * argv[])
{
	int mat[DIM][DIM], i, j, ris;

	for (i = 0; i < DIM; i++) {
		for (j = 0; j < DIM; j++) {
			scanf ("%d", &mat[i][j]);
		}
	}

	ris = proprieta_matrice (mat);
	printf ("%d\n", ris);

	return 0;
}

int proprieta_matrice (int mat[][DIM])
{
	int ris, i, j;

	ris = 1;
	for (i = 0; i < DIM && ris; i++) {
		for (j = 0; j < DIM && ris; j++) {
			if (mat[i][j] < START || mat[i][j] > END) {
				ris = 0;
			}
		}
	}

	return ris;
}