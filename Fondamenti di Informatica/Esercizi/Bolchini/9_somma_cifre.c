/*Acquisire un valore, quindi calcola e visualizza la somma delle cifre che lo compongono*/
#include <stdio.h>
#define BASE 10

int main(int argc, char * argv[])
{
	int val, ris, tmp;
	scanf("%d", &val); /* mettiamo 18 */
	if (val >= 0) {
		tmp = val;
	} else {
		tmp = -val; /* fa il valore assoluto e tmp serve solo a non sovrascrivere val */
	}
	ris = 0;
	while (tmp > 0) {
		ris = ris + tmp % BASE; /* ris = 0 + 8 che è il resto della divisione tra 18 e 10 */
		tmp = tmp / BASE; /* tmp = 18 / 10 = 1 */
	} /* poi comincia il secondo giro e viene 8 + 1 (resto della divisione tra 1 e 10) e tmp = 0 quindi ris = 9 */
	printf("%d\n", ris);
	return 0;
}