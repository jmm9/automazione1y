/* Scrivere un programma che riceve da riga di programma la lunghezza di
due sequenze di valori interi (la lunghezza è la stessa e viene indicata una
sola volta) e un intero che vale 1 o 0 per indicare se verrà anche fornita
una sequenza (della stessa lunghezza) di valori reali (pesi). Il programma
acquisice i dati successivamente inseriti dall'utente e chiama il sottoprogramma
distanza che calcola la distanza euclidea. Se l'utente ha fornito i pesi
verrà visualizzta la distanza pesata [radq(sommatoria(p(i)*(v(i)-u(i))^2))],
altrimenti quella normale. */
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#define OPTIONS 3

void distanza (int [], int [], float [], int, float *, float *);

int main (int argc, char * argv[])
{
	int dim, i, val, pesi_bool;
	int *obs1, *obs2;
	float de, dep;
	float *pesi;
	
	if (argc == OPTIONS) {
		dim = atoi (argv[1]);
		pesi_bool = atoi (argv[2]);
		if ((obs1 = malloc (dim * sizeof (int)))) {
			if ((obs2 = malloc (dim * sizeof (int)))) {
				for (i = 0; i < dim; i++) {
					scanf ("%d", (obs1 + i));
				}
				for (i = 0; i < dim; i++) {
					scanf ("%d", (obs2 + i));
				}
				if (pesi_bool) {
					if ((pesi = malloc (dim * sizeof (int)))) {
						for (i = 0; i < dim; i++) {
							scanf ("%f", (pesi + i));
						}
						distanza (obs1, obs2, pesi, dim, &de, &dep);
						printf ("%f\n", dep);
						free (pesi);
					} else {
						printf ("Errore nell'allocazione di %d numeri reali\n", dim);
					}
				} else {
					pesi = NULL;
					distanza (obs1, obs2, pesi, dim, &de, &dep);
					printf ("%f\n", de);
				}
				free (obs2);
			} else {
				printf ("Errore nell'allocazione di %d interi (sequenza 2)\n", dim);
			}
			free (obs1);
		} else {
			printf ("Errore nell'allocazione di %d interi (sequenza 1)\n", dim);
		}
	} else if (argc < OPTIONS) {
		printf ("Error: too few arguments\n");
	} else {
		printf ("Error: too many arguments\n");
	}

	return 0;
}

void distanza (int u[], int v[], float p[], int dim, float *dist, float *dist_p)
{
	int i, diff;
	float diff_p;

	if (p) {
		diff = 0;
		for (i = 0; i < dim; i++) {
		diff += (p[i] * (u[i] - v[i])) * (p[i] * (u[i] - v[i]));
		}
		*dist_p = sqrt (diff);
	} else {
		diff_p = 0.0;
		for (i = 0; i < dim; i++) {
			diff_p += (u[i] - v[i]) * (u[i] - v[i]);
		}
		*dist = sqrt (diff_p);
	}

	return;
}