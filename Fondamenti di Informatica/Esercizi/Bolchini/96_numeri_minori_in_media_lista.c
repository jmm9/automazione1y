#include <stdio.h>
#include <stdlib.h>
#include "list_int.h"
#define NMAX 80
#define STOP 0

lista_t* minoriInMedia (int [], int);
float media (int [], int);

int main (int argc, char * argv[])
{
	lista_t *head = NULL;
	int seq [NMAX], i, val;

	scanf ("%d", &val);
	for (i = 0; val != STOP; i++) {
		seq [i] = val;
		scanf ("%d", &val);
	}

	head = minoriInMedia (seq, i);

	printl (head);

	head = empty (head);
	
	return 0;
}

lista_t* minoriInMedia (int seq[], int dim)
{
	lista_t *head = NULL;
	float avg;
	int i;
	
	if (dim) {
		avg = media (seq, dim);
		for (i = 0; i < dim; i++) {
			if (seq[i] <= avg) {
				head = append (head, seq[i]);
			}
		}
	} else {
		printf ("Non ci sono valori\n");
	}
	
	return head;
}

float media (int seq[], int dim)
{
	int tot, count;
	float avg;

	tot = seq[0];
	for (count = 1; count < dim; count++) {
		tot = tot + seq[count];
	}
	avg = (float) tot / count;

	return avg;
}