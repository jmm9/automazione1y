#include <stdio.h>
#define LEN 10

int quanteCombinazioni (int [], int, int);
/*int quanteCombinazioni_r (int [], int, int);*/

int main(int argc, char * argv[])
{
	int input [LEN], i, ris, trg;

	printf ("Inserisci i 10 numeri\n");
	for (i = 0; i < LEN; i++) {
		scanf ("%d", &input[i]);
	}
	printf ("Inserisci il numero target strettamente positivo\n");
	do {
		scanf ("%d", &trg);
	} while (trg <= 0);

	ris = quanteCombinazioni (input, LEN, trg);

	printf ("Ci sono %d combinazioni di numeri che sommati danno il target\n", ris);
	return 0;
}

int quanteCombinazioni (int input[], int dim, int trg)
{
	int i, j, z, k, y, ris, supporto [LEN-1], tmp;

	ris = 0;
	for (i = 0; i < dim; i++) {
		if (input [i] == trg) {
			ris++;
		} else {
			for (j = 0; j < dim && j != i; j++) {
				tmp = input [i] + input [j];
				if (tmp == trg) {
					ris++;
				} else if (tmp < trg) {
					supporto[0] = tmp;
					for (z = 0; z < dim && z != j && z != i; z++) {
						for (k = 1; k < (dim--); k++) {
							supporto[k] = input[z];
						}
					}
				/*	for (y = 0; y < dim--; y++) {
						printf ("%d ", supporto[y]);
					}*/
					printf ("\n");
					ris = ris + quanteCombinazioni (supporto, dim-1, trg);
				}
			}
		}
	}

	return ris;
}

/*int quanteCombinazioni_r (int input[], int dim, int trg)
{
	int j, z, k, y, ris, supporto [LEN-1], tmp;

	ris = 0;
	for (j = 0; j < dim; j++) {
		tmp = input [0] + input [j];
		if (tmp == trg) {
			ris++;
		} else if (tmp < trg) {
			supporto[0] = tmp;
			for (z = 0; z < dim && z != j; z++) {
				for (k = 1; k < dim--; k++) {
					supporto[k] = input[z];
				}
			}
			/*for (y = 0; y < dim--; y++) {
				printf ("%d ", supporto[y]);
			}
			printf ("\n");*//*
			ris = ris + quanteCombinazioni_r (supporto, dim-1, trg);
		}
	}
	
	return ris;
}
*/




















