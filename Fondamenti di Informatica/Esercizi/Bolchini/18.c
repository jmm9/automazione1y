/* Prendo 50 valori massimo finché l'utente non mette qualcosa di minore o uguale a zero. Stampa 1 se non ci sono numeri primi, altrimenti 0 */
#include <stdio.h>
#define NMAX 50
#define BASE 10
#define STOP 0

int main(int argc, char * argv[])
{
	int val[NMAX], i, j, h, dim, num, resto, ris, noprimo;
	dim = 0;
	scanf("%d", &num);
	while(num > STOP) {
		val[dim] = num;
		dim++;
		scanf("%d", &num);
	}
	noprimo = 0;
	for (i = 0; i < dim; i++) {
		h = 0;
		for (j = 2; j < val[i]; j++) {
			resto = val[i] % j;
			if(resto == 0) {
				h++;
			}
		}
		if(h != 0) {
			noprimo++;
		}
	}
	if(noprimo == dim) {
		ris = 1;
	} else {
		ris = 0;
	}
	printf("%d\n", ris);
	return 0;
}
