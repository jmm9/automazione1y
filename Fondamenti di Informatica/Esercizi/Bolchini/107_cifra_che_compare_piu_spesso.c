#include <stdio.h>
#define BASE 10

int maxOccorrenze (int);

int main (int argc, char * argv[])
{
	int num, ris;

	scanf ("%d", &num);
	ris = maxOccorrenze (num);
	
	printf ("%d\n", ris);
	return 0;
}

int maxOccorrenze (int num) {
	int occorrenzaCifre [BASE], resto, i, maxCifra;

	for (i = 0; i < BASE; i++) {
		occorrenzaCifre[i] = 0;
	}

	while (num > 0) {
		resto = num % BASE;
		num = num / BASE;
		occorrenzaCifre[resto]++;
	}

	maxCifra = 0;
	for (i = 1; i < BASE; i++) {
		if (occorrenzaCifre[i] >= /* Anche uguale perché da specifica vuole la più grande in caso di parità */ occorrenzaCifre[maxCifra]) {
			maxCifra = i;
		}
	}

	return maxCifra; /* Cifra che compare più spesso */
}
