/* Restituisce un array con solo valori che hanno il campo val diverso dal precedente */
#include <stdio.h>
#include <stdlib.h>
#define MAXLEN 100

typedef struct dato_s {
	int val;
	long istante;
} dato_t;

dato_t * filtraValori (dato_t [], int, int*);

int main (int argc, char * argv[])
{
	dato_t input [MAXLEN];
	dato_t *ris;
	int dim, dim_ris, i;

	for (i = 0; i < MAXLEN; i++) {
		scanf ("%d%l", &input[i].val, &input[i].istante);
	}

	ris = filtraValori (input, dim, &dim_ris);

	if (ris) {
		for (i = 0; i < dim_ris; i++) {
			printf ("%d %l\n", ris[i].val, ris[i].istante);
		}
		free (ris);
	} else {
		printf ("Acquisizione: errore nell'allocare memoria per %d interi\n", dim_ris);
	}

	return 0;
}

dato_t * filtraValori (dato_t campioni[], int numeroCampioni, int *dim_ris)
{
	int i, last, cur;
	dato_t *campioniFiltrati;

	*dim_ris = 1;
	last = campioni[0].val;

	for (i = 1; i < numeroCampioni; i++) {
		if (campioni[i].val != last) {
			*dim_ris++;
			last = campioni[i].val;
		}
	}

	if (campioniFiltrati = malloc (*dim_ris * sizeof (dato_t))) {
		campioniFiltrati[0] = campioni[0];
		last = campioni[0].val;
		cur = 1;
		for (i = 1; i < numeroCampioni; i++) {
			if (campioni[i].val != last) {
				campioniFiltrati[cur] = campioni[i];
				last = campioni[i].val;
				cur++;
			}
		}
	}

	return campioniFIltrati;
}
